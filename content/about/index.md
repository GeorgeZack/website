---
title: About
layout: about
---

## About Me

My name is George Zackrison

<!-- 	      ##### ## -->
<!-- 	   /#####  /## -->
<!-- 	 //    /  / ### -->
<!-- 	/     /  /   ### -->
<!-- 	     /  /     ### -->
<!-- 	    ## ##      ##    /##    ##   ####        /### -->
<!-- 	    ## ##      ##   / ###    ##    ###  /   / #### / -->
<!-- 	    ## ##      ##  /   ###   ##     ###/   ##  ###/ -->
<!-- 	    ## ##      ## ##    ###  ##      ##   #### -->
<!-- 	    ## ##      ## ########   ##      ##     ### -->
<!-- 	    #  ##      ## #######    ##      ##       ### -->
<!-- 	       /       /  ##         ##      ##         ### -->
<!-- 	  /###/       /   ####    /  ##      /#    /###  ## -->
<!-- 	 /   ########/     ######/    ######/ ##  / #### / -->
<!-- 	/       ####        #####      #####   ##    ###/ -->
<!-- 	# -->
<!-- 	 ## -->
<!-- 	     ##### /      ##                 ###                ### -->
<!-- 	  ######  /    #####                  ###                ### -->
<!-- 	 /#   /  /       #####                 ##        #        ## -->
<!-- 	/    /  ##       / ##                  ##       ##        ## -->
<!-- 	    /  ###      /                      ##       ##        ## -->
<!-- 	   ##   ##      #      ##   ####       ##     ########    ## -->
<!-- 	   ##   ##      /       ##    ###  /   ##    ########     ## -->
<!-- 	   ##   ##     /        ##     ###/    ##       ##        ## -->
<!-- 	   ##   ##     #        ##      ##     ##       ##        ## -->
<!-- 	   ##   ##     /        ##      ##     ##       ##        ### / -->
<!-- 	    ##  ##    /         ##      ##     ##       ##         ##/ -->
<!-- 	     ## #     #         ##      ##     ##       ## -->
<!-- 	      ###     /         ##      /#     ##       ##         # -->
<!-- 	       ######/           ######/ ##    ### /    ##        ### -->
<!-- 	         ###              #####   ##    ##/      ##        # -->
<!---->
<!-- ## &#8284; God Wills It! &#8284; -->
<!---->
<!-- I'm a fan of the Crusades, the medieval era, and the early modern period. -->

I'm a member of the Church of Jesus Christ of Latter-day Saints. You may
recognize the term "Mormon" because that is a nickname given to members. It's
also a book of scripture that we hold sacred in our religion, the *Book of
Mormon*.

I'm a computer programmer, fan of history, enjoyer of religion, player of
tabletop roleplaying games, and amateur reader. I appreciate Linux (i use artix
btw) and Open-Source tools.

I'm a fan of pulp authors like H.P. Lovecraft.

{{< img src=./images/1921-A.jpg caption=`H.P. Lovecraft smiling with a friend. A
rare but sweet sight.` width=400px >}}

And Robert E. Howard. Especially his tales of the legendary English Puritan,
*Solomon Kane*.

{{< img src=./images/Blue_Flame_Vengeance.jpg caption=`A drawing taken from the
story Blue Flame of Vengeance.` width=400px >}}

I highly recommend reading them, the King James Bible, and *The Worm Ourboros*
to appreciate what good English can feel like.

Contact me by email at
[zackrisongeorge@disroot.org](mailto:zackrisongeorge@disroot.org) or by pressing
the button that looks like this in the footer: {{< img src=/email.gif width=60px
>}}

[The Internet Makes You Stupid](https://youtu.be/SMp4uwjOg9Q); well and it makes
you smart. It depends...

