---
title: First Notice
date: 2023-07-31T20:46:00-05:00
author: Grand Duke Julian Seara
duchy: Seara
pronoun: His
---

## Renaming of characters required
Humans are required to use Elizabethan names, not the Germanic names previously
requested. This is due to the change in the background material. Elves and
Dwarves will use typical fantasy name tropes.

Name generators can be found here:
- [Elizabethan Names](https://www.fantasynamegenerators.com/elizabethan-names.php)
- [Elven Names](https://www.fantasynamegenerators.com/elf-names.php)
- [Dwarven Names](https://www.fantasynamegenerators.com/dwarf-names.php)

## Gunpowder!
Gunpowder will be added to the game. This is early gunpowder, circa 1500,
nothing fancy. Will be very slow, very loud, very smokey, but otherwise an
effective weapon. Dwarves like them---and potentially invented them---while
Humans have made them into efficient killing machines for the large battles
occurring in the world.

I need to find good gunpowder rules, prices, and such still, but otherwise
gunpowder is coming to a session soon!

## Demi-humans
Moldvay rules dictates race as class. We have one elf player currently. I think
I will preserve the abilities and simply port them over to the new rules that
are currently in the works. No changes are necessary at this time.

## Setting material
I will draft a small introductory article on what the characters would
reasonably know about the world around them, the political situation, the common
knowledge of recent historical events, and other such items.

One thing that I want to see is players really treating this setting as a
sandbox. A sandbox, by definition, is pliable. Sand can be shaped and formed and
destroyed. Think about this when you are in and out of character, or in and out
of session. Would characters with enough wealth and power try to create a pirate
republic in the sea? Would they establish a mission in the Dwarven kingdom?
Would they preach their religion to pagans? Would they build up their own
kingdom and attempt a coup?

Level 1 characters wouldn't do any of these things, but they may *want* to do
these things. Enough wealth and power could make these things possible! Talk to
me! Let's discuss what you want to do and make it happen. I won't automatically
grant such desires, but I certainly would be interested in exploring a
reasonable and balanced approach that leads to such outcomes but is still not
guaranteed. Countless kings in history failed to build their empire, so why
would your character be an exception to such rules? But it is also a fantastic
world of heroes and villains. Maybe, if the character survives long enough, they
actually can become a powerful political entity. Or perhaps they get
assassinated and poisoned. Who knows!? It'd be fun to try either way!

## Appendix N
Appendix N resources will be coming soon. Appendix N refers to the appendix
chapter in the Original Edition of Dungeons and Dragons, where Gary Gygax and
Dave Arneson, authors of the rules, listed inspirational material. Such material
included many many books. Mine will include the same, but also images, videos,
and articles that have inspired the setting.
