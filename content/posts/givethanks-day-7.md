---
title: "#GiveThanks Day 7 - The Plan of Happiness"
date: 2023-05-14T22:47:25-05:00
tags: ['givethanks', 'religion']
---

## Day 7
### I am thankful for this Life and the Plan of Happiness
#### Christ Jesus enables us to change, God grants us justice and mercy, and the Holy Ghost comforts us

> For behold, this is my work and my glory—to bring to pass the immortality and
> eternal life of man.

-- Moses 1:39

If you've ever watched the six ~~only good~~ original and prequel movies of Star
Wars, you'll know the story of Anakin and Luke Skywalker. The story to me is
about redemption and fate. How the fate of Anakin was to change the galaxy and
overcome evil, the fate of Luke to become a Jedi like his father and to embrace
his fate. But of course those two fates become intertwined and complicated.
Anakin becomes an icon of evil, of the Sith, before he becomes truly good. Luke
struggles to keep what friends he has before relinquishing the control to
embrace his fate. Ultimately, the son saves the father, and the father is the
true hero of the story. Luke's mission was not to be the hero, as he originally
thinks, but to simply help his father. In the end Anakin restores balance to the
Force and kills the Emperor, not Luke.

In the every end of the series we see Luke burning his father's body, and music
begins to play. The music is a little silly, but it is also triumphant and
celebratory. It is a good song to represent a good moment. Victory! I think of
this song often when I think of life and the Plan of Salvation. It is won
already! What good news! The music starts out strange but the final moments are
what really make it grand:

{{< youtube II04E2GEJG8 >}}

I think it is supremely relevant to what I am grateful for this day. This life
is full of challenges and paths. But in the end, we can rest assured that
victory has been won. Our jobs are not to save the world, this has been done
already. Jesus Christ has saved us and the world, He has given us everything at
the direction of His Father. Our role---our fate---is simply to accept Him and
emulate Him. We need not worry about the weight of a world, it is handled
already.

When Moses was caught up in an exceedingly high mountain---a
Temple---transfigured He saw the face of God in the flesh. He spoke with God.
God states the verse I previously quoted. This Plan of Salvation, of Happiness,
the Atonement, all of these grand and important things pertaining to our
Eternal lives are all for us. They are for our life. God loves us, He sent His
Son, the Holy Ghost bares record of all things to us. We have been saved. The
Kingdom shall stand forever, it is up to us to embrace it. God shall not be
destroyed nor the Savior's Atonement undone, that is not the way of this life.
But we can reject God and the Atonement of Jesus Christ. So it is up to us to
seek our fates on this Earth. It is up to us to endure to the end.

{{< img src=../images/Jesussmiling.jpg caption=`Jesus Christ loves us all` >}}

Will we change our ways? Will we grant mercy? Although it is a silly comparison,
Star Wars has some important lessons. Star Wars is essentially the story of the
hero. Jesus Christ is the hero and has overcome hell. Our role is much like
Luke's in the sense that we are not the protagonist of the story---that belongs
to Christ---but we are the protagonist of *our* story. Our role is also much
like Anakin that we must change and repent, to overcome the evil in *our* lives.
So will we? Will we change? I am grateful that God has given me the chance to
change, that Jesus Christ has fulfilled the Atonement, and that the Holy Ghost
can prompt me always to know what is right and what is wrong.

> Wherefore teach it unto your children, that all men, everywhere, must repent,
> or they can in nowise inherit the kingdom of God, for no unclean thing can
> dwell there, or dwell in his presence; for, in the language of Adam, Man of
> Holiness is his name, and the name of his Only Begotten is the Son of Man,
> even Jesus Christ, a righteous Judge, who shall come in the meridian of time.

--Moses 6:57
