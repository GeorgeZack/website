---
title: "#GiveThanks Day 2 - Jesus Christ"
date: 2023-05-09T21:24:30-05:00
tags: ['givethanks', 'religion']
---

#### Following President Nelson's challenge

This last Sunday, during Gospel Study class, we discussed the Come Follow Me
lesson for the week. At the end, the teacher showed the video of President
Nelson asking the Church to share something they are thankful for each day for
seven days. This was originally back in November of 2020 during the height of
the Covid-19 pandemic. Although this is late for the original challenge, I will
follow the teachers prompt and write what I am thankful for here for this week.

## Day 2
### I am thankful for Jesus Christ
#### The Messiah, Savior of all Mankind, the Anointed One

Jesus Christ is the Light of the World. He is the way to our Father. Through Him
we can all be saved. We can change from our erroneous ways and refined into a
more perfect being. Through Christ's atonement we can change each day, through
the Sacrament we can renew our covenants and become better examples of Him. May
we all bless each other by emulating Christ. With Christ, all things are
possible.

{{< img src=../images/come-follow-me-michael-malm.jpg caption=`Come Follow Me by
Michael Malm` >}}

I once had an experience talking to a Jewish tour guide who was taking me
through the Old City of Jerusalem. Inside the Church of the Holy Sepulcher, he
pointed to the traditional spot of the Crucifixion and said, "This is where
Christianity was born." That is not correct. It was born when Christ rose from
the tomb on the Third Day. After Hell was harrowed and the dead Saints were
organized and preached to by Christ, He reemerged from the tomb and brought with
Him the conquest of death.

{{< img
src=../images/Ivan_Kramskoy_-_Христос_в_пустыне_-_Google_Art_Project-min.jpg
caption=`Painting by Ivan Kramskoy` >}}

In Christ Jesus all can be reborn. All can change. All things pass away in the
Atonement of Jesus Christ. His Grace, His Work, His Resurrection, all of it is
more than enough for us. Each man and each woman on the Earth has been saved.
All the dead in their graves shall rise and praise Jesus' name. All knees shall
bow and tongues confess that He is the Christ. He is the Messiah. He is the King
of Kings and the Only Begotten. Without Him all is naught, no plan of Salvation
or Happiness can exist.
