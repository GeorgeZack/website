---
title: "My First Post"
date: 2022-07-31T21:22:30-05:00
tags: ['general', 'web', 'unix']
---

## I HATE THE ANTICHRIST

(aka JavaScript soydev frameworks)

### Why am I making yet another website?

Looking back at some internet archive snap-shots of my site, I can't help but
reflect on the amount of technologies I tried to use that diverged from the very
first site I built. In 2014-2015 I built my first website using HTML, CSS, and a
small amount of JavaScript. It started as a very simple site in a HTML class
that I had to take in High-School. After that it had more content, long since
gone (I didn't use Git at the time) and eventually I was getting more fixated on
interactivity. I wanted my site to have a guest book, so I started using PHP.
PHP left a bad taste in my mouth and I try to avoid ever using it. Slowly, after
working as a programmer for a year or so I moved the technology over to React.

This was the firs real investment in my website and I began testing mobile as
well. This was all around 2018-2019. But I got sick of writing so much overhead
code just to display some text. So I changed again. This time I started using
Svelte and Svelte-Kit. They did me well until I stopped working on the site for
a while and after coming back I tried to update the packages. It broke, of
course, as all Node/NPM projects do. This is when my hate for soydev frameworks
began to spill over. That was 2021-2022. Now near the middle of 2022 I have this
site.

This uses Hugo, which takes Markdown, template, and other static files and spits
out a great site with them. It's more like a normal programming approach, rather
than spitting out hundreds of minified lines to do simple tasks, it produces
several HTML files. It's legible and simple. I like it.

## SoyDevery at work

Soydev shenanigans leads to poor development habits. I would debug for hours on
my React or Svelte site, pouring over example code and documentation to figure
out what I wanted my site to do. But I didn't know what I was doing or how the
framework was doing things in the background. All the added stress of these
extra files and complications lead to me losing it and look into building a
website generator via Bash!

Soydev's like complexity. But I think everyone knows, deep down, that genius is
often expressed in simple terms. That simple things attract genius, while
complexity attracts retards who don't know how it works and, therefore, think it
must be good.

## UNIX opposition

This makes even the simplest applications more convoluted than is necessary! The
modern web is bloated and annoying to navigate. I like the simplicity of Hugo
(thank you Luke Smith for showing it off in a video he made) and appreciate that
it makes things simple for me. I have vowed that in the future, if I need a
purpose built app I will purpose build it. The UNIX philosophy of having small
but interchangeable programs that can communicate with each other is genius
because it is simple. If I want to make a calculator for generating player
characters in a TTRPG, then I will build a program for it. Rather than trying to
do everything on my website (the mistake that bombed my website many times
over)!
