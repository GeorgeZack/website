---
title: "Magic and Technology"
date: 2023-05-15T08:25:10-05:00
draft: true
---

## A Rivalry of Humanity and Gods

What do we do when the world is descending into madness?

[Terry Davis speaking to the issues of modern technology](https://youtu.be/k0qmkQGqpM8)

With the popularization of AI and the trend towards technological progress with
little regard to consequences, what can glean from the change? I believe the
stories of our ancestors can tell us much.
