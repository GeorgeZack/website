---
title: "#GiveThanks Day 4 - Ancestors"
date: 2023-05-11T22:46:06-05:00
tags: ['givethanks', 'religion']
---

## Day 4
### I am grateful for my Ancestors
#### Hwæt! Listen!

> Ne sorga, snotor guma;    selre bið æghwæm  
> þæt he his freond wrece,  þonne he fela murne.  
> Ure æghwylc sceal         ende gebidan  
> worolde lifes;            wyrce se þe mote  
> domes ær deaþe;           þæt bið drihtguman  
> unlifgendum               æfter selest.  

> Grieve not, wise king!    Better it is  
> for every man             to avenge his friend  
> than mourn overmuch.      Each of us must come  
> to the end of his life:   let him who may  
> win fame before death.    That is the best  
> memorial for a man        after he is gone.  

-- Beowulf, 1384-1389, translation by Howell D. Chickering, Jr.

I debated as to the order of the days of thanksgiving. So far I have gone in a
concrete order. Day four would naturally be *Scriptures*, but after writing
about the Holy Ghost I felt strongly compelled to speak to something that I am
deeply grateful for. Something that has marked my entire life. I love my
ancestors. I love them all with such a love that binds me---their son---to my
parents of all generations of the earth that have passed away.

{{< img src=../images/beowulf_fighting_the_dragon.jpg caption=`Beowulf may be
fictional, but he is a hero. His heroic tale was wrought by my fathers and so I
shall read it and retell it!` >}}

My kindred dead have given me so very much. From the horse riding, Steppe
dwelling, and conquering Indo-Europeans to the tall fair haired Nords and their
longboats afar, I have been blessed with a noble ancestry. Not better or worse
than any other, but mine. Through them I am here. Their works are manifest in
the culture that I have now and I honor them by learning of their culture, their
stories, their lives, good and bad.

But more than that. As they wander in the after life not all of them know the
truth. Not all of them are yet saved, lacking the saving ordinances that have
been revealed to us only relatively recently. So very many generations have past
to dust without full knowledge. They are waiting for a balm, wandering the
spirit world.

{{< img src=../images/jack-jones-helheim-smaller-file.jpg caption=`Helheim, by
Jack Jones` >}}

Hwæt! All is not lost! God, in His mercy, allows me to save them. And I will do
what I can to save them. It brings me to tears to think that I can help my pagan
ancestors come to know Christ Jesus. That they can be baptized, endowed, and
sealed through me. I can act as proxy and bless their afterlife in ways that
were not open to them in their living life.

Their music, their stories, their journeys, all the lives they led, bless me. I
am not alone in this life. They are with me. And I celebrate them in all ways I
can. I honor them, remember them, pray for them, and when the time comes, help
save them. Enjoy some tunes from Harald Foss, a modern musician retelling
ancient tales in bardic fashion:

{{< youtube _zl3i2GqCqA >}}

\
{{< youtube 5YzEA_uQBg4 >}}
