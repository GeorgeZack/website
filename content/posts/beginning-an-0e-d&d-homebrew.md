---
title: "Beginning a OD&D Homebrew"
date: 2022-09-05T23:17:43-05:00
tags: ['rpgs']
---

## This website has a lot of stuff

I'm currently working on writing several religion articles and exploring Rust,
Go, C, and expanding my expertise in GNU/Linux. But one passion that I also have
is _Dungeons & Dragons_.

Or rather, I like tabletop role-playing games, not just D&D. I started with
_Dungeons & Dragons_ 5e (fifth edition) running the official "Curse of Strahd"
module. I disliked it. It took time to realize _what_ I was doing wrong, and
perhaps my taste of 5e was tainted by _my_ mistakes. (Hopefully my players don't
regret the time they spent going through that campaign!)

I say that I started with 5e, like many others, but indeed that was starting _in
earnest_, in reality I actually bought the unfortunate 4e Red Box/Starter Set. I
didn't have a clue what I was doing---being a typical dumb middle-school
student without any guidance from grognards---and so I eventually (quite quickly
actually) dropped that set and moved on to video games. Briefly I also played
_Hands of Destiny_, an indie RPG that uses playing cards instead of dice and is
generic enough to not have much in the way when creating your own world. It was
fine. But again I didn't know what I was doing and didn't stick with it.

The CoS (Curse of Strahd) campaign really pushed me into the hobby. Soon after
starting I found a certain user-group invite link which was still working and
that opened my eyes to the wonderful world of OSR. Most of the things I have
learned and used in my games (for example, I started using BFRPG with a large
group of teenagers) I owe to that user-group. The users there have taught me
much about how old school RPGs were played and why they're worth using and
exploring today.

After the CoS campaign ended, I started exploring BRP games, specifically
Renaissance Deluxe. I love it... but I have realized that I prefer to not use
skill systems. BRP/Renaissance Deluxe features a quite expansive and
customizable skill system. Instead, I have come around to the old-school 1 in 6
simplicity. As much as I love the skill systems and D% rolling of BRP (it's
transparency and ease-of-reading is so cool) it takes me too long to make
characters. Perhaps in time I could make it easier, and in fact I began to write
a calculator application to automatically create characters, but until then I
love how quick it is to make characters in BX/0e games.

This current, 0e, homebrew actually started from a custom RPG system that I
wanted to build which would combine percentile skills but ease the character
creation process. My idea was to start every skill out at 15% and allow the
players to invest 200 or so points into the skills.

Another issue in the BRP games was the charge on the GM to find a skill for the
characters to use. Maybe I'm dumb but I couldn't figure out a good way to call
for tests that included either: a) the correct skill or b) multiple appropriate
skills. Usually what happens for me is that I'd look at the situation that
required some sort of random chance and I'd go to look for a skill from the
skill list. After spending a moment looking for the skill I would ask for a
roll. The player would then roll and say what kind of success they had. This
wasn't too bad most of the time, but there were too many edge cases for me that
it became a stumbling block. Again, I'm new to the hobby and I am still
learning! So maybe more experience would help me figure out a good way to figure
this out.

_As an aside, I enjoy Morrowind. It essentially uses a Runequest/BRP system as
it's underlining role-playing system. This is what originally got me interested
in BRP in the first place. Renaissance Deluxe came to my attention because of my
love for historical wargaming and the possibility of having a historical RPG.
Morrowind uses a few unique characteristics (instead of Size, and Wisdom, etc.)
and limits the skill system to a pretty small list which makes sense for a video
game. I thought that was wonderful when I figured that out, but after running
several sessions I realized that, though it is really cool to roll D% and to use
fun skills, it is not so fun to create characters. Again, perhaps I'm just a
dummy! But OSR character creation is usually such a breeze that I can't ignore
it._

Recently, I have been enjoying reading the original LBB (Little Brown Books)
version of D&D, otherwise known as 0e or OD&D. It's difficult to understand at
times, not because of poor writing but because it requires knowledge in
wargaming. I happen to have that, lucky me, but I lack RPG experience. Being in
the hobby for a mere year hardly qualifies me to understand _exactly_ what Gygax
and Arneson were meaning when they wrote the rules. But I think diving in and
_trying_ to understand has been just as useful for me.

I love how it is abstract but has emphasis in verisimilitude play. Also, there
are not complicated rules[^1] Instead, the simplicity of the system and abstract
combat allows play to be quick for me as the DM. Gygax famously mentions the
importance of time and time-tracking. This is in the game too. But it is not a
complicated thing. Instead it is a system that you can (and should!) add to a
campaign.

This lead me to realize the importance of adding and using good systems in the
game. A game should be full of good systems that add to the verisimilitude but
do not add weight and slow things down _too_ much. Of course it is a natural
consequence to add weight when adding systems. But Gygax speaking on time seems
to emphasize the importance of putting player characters into risk vs. reward
situations. Any additional system should contribute to that tenant, and
therefore encumbrance, though it can be annoying at times to track, should be
used! But honestly I think it's hardly difficult to keep track of a few numbers
and slots.

Time should be tracked so like a resource to push players to ask themselves "Is
it worth it to stay in the dungeon for 3 more rooms? Do we have enough light?
But we could get to the treasure room and get a hansom reward... Ah what do we
do?!" That situation is more immersive and pushes the players more than any
artificial/railroad can. That was one thing that disappointed me with 5e/CoS.
There was little incentive to the players to do things other than them doing the
thing because that's what the DM prepared for the week, because the characters
are the heroes of the story, because blah blah blah.

Not all parties need to be gallant heroes---though I definitely prefer
that---but they should have an investment into the world. Characters who have an
investment in the world allows the players to have an investment in the game.
And that draws them in. At least, this is what _I think_ it should be
like. I'm not finished reading the manual so we'll see if I learn more that
changes what I have otherwise assumed or learned. I will update this page or
write another post to reflect my evolution. I also accept that I don't know what
I don't know, and that I'm a new entry into this expansive hobby. If you are so
inclined, send an email and straighten me out!

## The Homebrew...

Now that I have written what I've learned, now I'll mention what I'm working on.
I like to practice what I learn, or rather, I like to learn by practice and
experimentation. That's how I have (and continue to) learned programming. On the
job! I think it's the same with RPGs. So I'm writing an interpretation/homebrew
on top of 0e. It's going to have technology from the Early-Modern period (think
30 Years War, Three Musketeers, Jamestown, or Sid Meier's Pirates!) and will
appropriately fit rules and systems into the setting.

(I have to disagree with Gygax in the 1e DMG, I think the campaign/setting is 
the most important thing, not the rules. But that doesn't mean I can't mold the
rules to the setting.)

It will also be... classless... _gulp_! I want to give a different system a try.
I am calling it: _Callings_. During character creation a player can choose one
or more callings for their character to have. Whenever a situation occurs that
may include expertise learned from previous callings (the last calling being an
adventurer, hence the characters romping around killing monsters) they get a
bonus to the roll. Callings represent an extended amount of time dedicated to a
craft or a skill. Right now it's 10 years, but I might tweak the time commitment
for each calling.

Being classless would reflect the shifting mindset from the Medieval worldview
of _you are what you do_ into _you are your abilities_ of the Renaissance. For
example, Doctors used to be simply people who doctor, or who heal. In the
Renaissance this began to shift, you could become a Doctor in other fields. It
became an indicator of what you knew _and_ what you could do. Perhaps this is an
unnecessary addition to the rules. I know that the holy trinity of Fighting Man,
Cleric, and Magic User would be broken.

I am also contemplating either removing HP increases per level, or significantly
reducing the increase per level. I want the game to be deadly and also to
represent that any weapon or gun could kill a man instantly in this time period.
I understand that more hit die represent fighting ability **and** vitality, and
it's not just an indicator of health. So I need to experiment with it. Perhaps
I'll have wounding that has a chance to instantly kill characters. I'm not sure
yet.

But we will see if this works. If it does, it will bring back the feeling of
freedom and deadliness I enjoyed from Renaissance Deluxe. I loved how wounding
worked in that game too!

I know the title of this post indicates that I'd go over the rules in more
detail than I have so far, but this has turned out to be a long post. I think I
will end it here and add additional part(s) in the future which explain what I'm
doing with it and the departures from the base rules.

Until next time!

[^1]: "Waaaa! What about THAC0? Waaaa!" --A typical 5e player sperging-out
