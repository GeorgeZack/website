---
title: "Observations on the Book of Mormon"
date: 2024-05-14T09:41:30-05:00
draft: true
---

I've been reading the Book of Morning with a few questions in mind and I wanted
to share some things I've learned.

## Lamanite Religion
One is about the religion of the Lamanites and how it relates to why Lehi fled
Jerusalem. Laman and Lemuel are usually characterized as angry atheists who
rebel against their parents,[^1] but later on in the Book of Mormon we get hints
of what they taught their descendants. They seemed to actually have been
faithful to the religion of Jerusalem and not Lehi. When Ammon preaches to
Lamoni, Lamoni asks about "The Great Spirit,"[^2] which again has been
characterized as some sort of Native American primitive spiritualism or
paganism. But the Lamanites have temples and synagogues[^3]--as opposed to the
Nephite churches, curiously enough[^4]--so we can deduce that they had a
complicated or structured religion. This is not true of modern paganism or the
American spiritualism encountered by White explorers.[^5] It seems to make sense
that they were adherents to the Hebrew religion which had undergone corruption
to the point that Lehi, a prophet, was forced to leave Jerusalem![^6]

Also, it's funny to me but I was reminded of popular Christianity when the Great
Spirit was mentioned. Ask a Baptist or Catholic the nature of God and you'll get
a very complicated answer that they don't understand (this is admitted and
accepted, that we humans can't understand God).[^7] But compared to Lehi and
Nephi's religion, God is simple to comprehend. He is a perfected man who
descends down to His children[^8] and lifts them up to Him.[^9] All these
religions, the ancient Hebrews, Jews, and Creedal Christians, all have a
corruption of the Truth, and a common corruption is the nature of God. This
leads me to my next point.

## Corrupted Texts
Recently I watched the 1956 film *The Ten Commandments* and I really enjoyed it.
It was funny though when Moses mentions that God was a spirit, and that the
"finger of the Lord" was a pillar of fire.[^10] I believe these
oddities--especially compared to our canon--demonstrate that the Biblical
narrative is not inconsistent, but rather edited. The Deutornomical
reformers[^11] [^12] went back and touched up the Moses account and likely
removed any sort of reference that the Book of Moses mentions, specifically of
the bodily and physical properties of God.[^13] Thank goodness for duplicate
documents like the Plates of Nephi[^14] and the Books of Moses and Abraham![^15]
In *The Ten Commandments* it seems to point to a traditional, edited, theology
and understanding of God, a creedal understanding of God. But the gifts of
Seer[^16] and Translator[^17] that belong to Prophets correct the wrong ways and
set us on a more perfect path.

[^1]:
[^2]:
[^3]:
[^4]:
[^5]:
[^6]:
[^7]:
[^8]:
[^9]:
[^10]:
[^11]: A group of Jewish scholars who edited the Bible at the time of King
Josiah and the exile. The books of Deuteronomy, Judges, Joshua, 1 and 2 Samuel,
and 1 and 2 Kings chiefly belong to them. See Kevin Christensen, "The
Deuteronomist De-Christianizing of the Old Testament," The FARMS Review 16, no.
2 (2004): 60.
[^12]: Margaret Barker, The Great High Priest: The Temple Roots of Christian
[^13]: Moses 1:2, 4, 31
[^14]: The Plates of Nephi of the Book of Mormon were an abridgment of the Book
of Lehi, which were lost with the 116 pages of the original manuscript. A copy
of the same story, or an abridgment, was a gift to us because of the 116 pages
being lost, that we could still have the same stories written out for us.
[^15]: See The Pearl of Great Price
[^16]: Amos 3:7, Moses 6:35-36
[^17]: Omni 1:25
