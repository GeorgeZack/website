---
title: "#GiveThanks Day 3 - Holy Ghost"
date: 2023-05-10T22:31:16-05:00
tags: ['givethanks', 'religion']
---

#### Following President Nelson's challenge

You get the drift now.

## Day 3
### I am thankful for the Holy Ghost
#### The Comforter, He who testifies to our hearts of the truth

The Holy Ghost is the third member of the Godhead, the most ethereal, but the
most present. He has borne testimonies into my heart to allow me to know the
truth of all things I believe in. Within my heart I have felt a sacred fire,
kindled by the Spirit, the distinct being who moves in ways unknown and speaks
in all tongues. Like sacred Magic changing the course of history he moves upon
the Saints to do works that are not possible otherwise.

{{< img src=../images/angel.jpg caption=`Magic and the Spirit work on all of us.
Image from Internet Sacred Text Archive` >}}

The Spirit especially has brought me to tears and has testified in ways that I
cannot fully explain. Breathing in the knowledge that He imparts is a sacred
experience and it cannot be adequately expressed with words on a page. Even
images fail to completely explain what goodness is felt within my heart. When I
read the Book of Mormon and prayed earnestly about it the Holy Ghost descended
upon me and bore witness of it's truthfulness to me that no other person can.
Nothing on this earth belong to those feelings.

{{< img src=../images/beyond.jpg caption=`There is more than what we can see.
Image from Internet Sacred Text Archive` >}}

My eyes have been opened by the tender feelings that have---and will
continue---hit me when I least expected and most needed them.

> Come, thou Fount of ev'ry blessing,  
> Tune my heart to sing thy grace;  
> Streams of mercy, never ceasing  
> Call for songs of loudest praise.  
> Teach me some melodious sonnet,  
> Sung by flaming tongues above;  
> Praise the mount! I'm fixed upon it,   
> Mount of thy redeeming love.  
> \
> Here I raise my Ebenezer,  
> Hither by thy help I'm come;  
> And I hope, by thy good pleasure,  
> safely to arrive at home.  
> Prone to wander Lord, I feel it,  
> Prone to leave the God I love;  
> Here's my heart, O take and seal it,   
> Seal it for thy courts above.   
> \
> Jesus sought me when a stranger,   
> wand'ring from the fold of God;  
> He, to rescue me from danger,  
> Interposed his precious blood.  
> Prone to wander, Lord, I feel it,  
> Prone to leave the God I love;  
> Here's my heart, O take and seal it,  
> Seal it for thy courts above.  
> \
> O to grace how great a debtor  
> Daily I'm constrained to be!  
> Let thy goodness, like a fetter,  
> Bind my wand'ring heart to Thee.  
> Prone to wander, Lord, I feel it,  
> Prone to leave the God I love;  
> Here's my heart, O take and seal it,  
> Seal it for thy courts above,  
> Seal it for thy courts above.  

-- Come Thou Fount of Every Blessing, arranged by Mack Wilberg.

The Spirit has been the flaming tongue to proclaim to my heart the truth. That
truth has set me free.

[^1]: 
[^2]: See 1
