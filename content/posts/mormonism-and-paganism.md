---
title: "Mormonism and Paganism"
date: 2022-09-01T17:29:14-05:00
tags: ['religion']
---

_Pagan in this article is not written as a derogatory label, but as a label for
Indo-European religions and their modern survivors._

## Preamble

This is a niche subject. You are a niche person for even reading this site. But
as you have guessed at this point, I am a member of the Church of Jesus Christ
of Latter-day Saints. We are sometimes referred to as Mormons, and our theology
Mormonism. (And we referred to ourselves as Mormons for a very long time until
recently.) This article is assuming that you are familiar with Mormon theology,
specifically three things:

* The pre-mortal origin and post-mortal fate
* The Temple---and the ordinances therein---and proxy ordinance work for our
	kindred dead
* Adam and Eve's time in Eden and that their partaking of the fruit of the
	knowledge of good and evil was **not** a sin

If you are not familiar than I highly suggest reading the Pearl of Great Price.
It goes over, in detail, the eternal lives that we have and the importance of
our Earthly experience. It also discusses truths that have been lost in the
Bible. Again, perhaps some of you protestants would be shocked that I say such a
thing, but I am assuming that you are familiar with the Church of Jesus Christ's
view that the Bible is correct _so long as it is translated correctly_ and that
there are more scriptures to be found---like the Book of Mormon, from which our
nickname comes---and that revelation can add to the canon of scripture.

The Pearl of Great Price is one such example of additional canon and was
revealed to Joseph Smith, the prophet of the Restoration, after he had published
the first edition of the Book of Mormon.

I also have a great deal of inspiration to attribute to [Survive the
Jive](https://survivethejive.blogspot.com) who did a wonderful video on the
Proto-Indo-European religion. He compiled many similarities of pagan religions
found throughout Indo-European peoples (Germans, Celts, Iranians, Indics, Slavs,
Greeks, etc.) and parsed the similarities. What he found was wonderful and
enlightening, and I am glad that my ancestors had such beliefs. I feel like the
video helped me understand my ancestors better and I am thankful for that.

## Shared Pagan Roots

Survive the Jive's [video](https://www.youtube.com/watch?v=exlcT_iL9-U) is
amazing and I highly recommend you watch that, you'll have a better
understanding of the subject. However I'll do a brief recap. Essentially, we can
trace many myths and pagan practices to a singular source. Somewhere in the
Central-Asian steppe, North of the Caucasus', the first of the Indo-Europeans
began to spread. With them came a religion that spoke of a world tree, of dogs
guarding the afterlife, of fates and gods that fought with evil.

Several things hit me over the head as I explored this topic. It is the belief
of the author of the video that all Indo-European belief systems come from a
common ancestor. One found in the Proto-Indo-European people. I thought that
these religions and the facts that struck me so would be worth talking about.

### The Rivers and the Tree

In the PIE (Proto-Indo-European) belief system, when our life ends our souls
carry on and pass into the afterlife. On the journey there, we pass by the
hounds who guard the way of any mortals. The souls go through a river and where
our memories are washed clean. Thus, reincarnation can occur. Wading across the
river or by boat is both possible, but those who wade may be drawn to the
deepest pits of hell, where snakes and serpents bite them. Like the tree, the
rivers flow throughout the universe and flow in and out of the worlds. The roots
of the tree are fed by this cosmological water.

I take this to be another way of saying "veil". The veil of forgetfulness
separates the pre-mortal and mortal worlds. Veils that do not convey
forgetfulness---merely veils of transition---exist between all worlds. We are
born into a body but lose our memory when we move from the pre-mortal life to
the mortal, Earthly life. We lose our body when we die and leave this Earth and
enter into post-mortal life. The transition from one world to another is
significant on our eternal journey.

The world tree is the connection between all worlds. The world tree is found
famously in Nordic cosmology. The tree reaches down to hell, where it's roots
lie and feed on the warm springs below, and it's branches reach far into heaven.
It ties all life together in one eternal round.

The tree that keeps the world in place, and which is gnawed at by snakes could
be from the same tree that a Cherubim guarded in the Garden of Eden. Or perhaps
the tree that Lehi saw in his vision documented in the beginning of the Book of
Mormon? Trees are often used to convey meaning in revelation and in scripture.
The entire book of Isaiah is dedicated to the grafting and mending of olive
trees.

Perhaps the tree in Eden was taken as the tree of Earth? And over time it
became the thing that held Earth connected to hell and heaven? After all, that
is true, the fruits of knowledge has led us to action. And actions have
consequences. One may choose hell or choose heaven.

Certainly the snake or dragon gnawing at the roots of the tree is Satan, the
great serpent whose head will be crushed by the Son of Man. Ragnarok, as
described by _some_ Norse stories (those stories were translated by Christian
monks in Iceland so it is unclear how true to the oral tradition they are)
includes the letting lose of the serpent. The serpent, or the Midgard Serpent,
will poison Thor before Thor thrusts the demon down. Similarly, Odin will be
consumed by Fenrir, the wolf of hell. The gods are consumed by the battle.

Loki is let loose as well, and we know as members of the Church that before the
Earth is translated to Celestial glory there will be a time of great trial and
when Satan's power will be unleashed. This will be the last time before he is
cast down into eternal damnation, as revealed in the Book of Revelations.[^1]

In Exodus 34 we read:

> And Moses called unto [the children of Israel]... And Moses talked with
> them... And till Moses had done speaking with them, he put a veil on his
> face... But when Moses went in before the Lord to speak with him, he took the
> veil off, until he came out. And he came out, and spake unto the children of
> Israel that which he was commanded. And the children of Israel saw the face of
> Moses, and the skin of Moses' face shone: and Moses put the veil upon his face
> again, until he went in to speak with him.

In Matthew 17:

> And after six days Jesus taketh Peter, James, and John his brother, and
> bringeth them up into an high mountain apart. And was transfigured before
> them: and his face did shine as the sun, and his raiment was white as the
> light.

In Ether 3:

> And it came to pass that when the brother of Jared had said these words,
> behold, the Lord stretched forth his hand and touched the stones one by one
> with his finger. And the veil was taken from off the eyes of the brother of
> Jared, and he saw the finger of the Lord; and it was as the finger of a man,
> like unto flesh and blood; and the brother of Jared fell down before the Lord,
> for he was struck with fear.

And in Section 110 of the Doctrine and Covenants:

> The veil was taken from our minds, and the eyes of our understanding were
> opened. We saw the Lord standing upon the breastwork of the pulpit, before us;
> and under his feet was a paved work of pure gold, in color like amber. His
> eyes were as a flame of fire; the hair of his head was white like the pure
> snow; his countenance shone above the brightness of the sun;

These are all significant encounters with our Lord. Jesus, in all of these
instances, shows Himself in complete glory. A being such as Jesus Christ---The
Jehovah, Alpha and Omega, The Only Begotten---has passed those rivers. He has
pierced the veils that darken us and exist in our journey. Crossing the river
and climbing the tree from the roots to the branches has transformed the mortal
Christ into the **Immortal Living One.**

He passes between worlds---up and down the tree of the worlds---as it suits Him
and exists in a way that we cannot fully understand. Such a being as that would
be difficult for us to comprehend. We would have to pass the rivers to the other
side of the afterlife to understand what He has endured. For now, on this middle
Earth, we can need veils and barriers to prevent us from being burned up in His
glory.

Are we not, in a sense, reincarnated? We are renewed and at rest when we pass
from the mortal life into the spiritual life afterwards. Could these be truths
obscured by time and by man's own workings?

## Takeaway

This is clearly aimed at other members of the Church, or at pagans. Christians
of different sects may find it hard to read this and disagree with so much. I
acknowledge that and realize that Christians and Mormons will likely not see eye
to eye on many things and this article was not to bridge that gap. Instead, I
want to speak to the members and to the neo-pagans.

**First to my fellow members.** We can appreciate what our ancestors did even
more if we keep in mind these facts. This religion of our ancestors---assuming
you're of Indo-European stock that is---is clearly a source of truth. The Bible
and the Book of Mormon are not the only truths that exist in the ancient world.
The accounts found inside both sets of scripture are unique, but I think they
are not wholly alone. I dream of the day that the Nordic Book of Mormon is
found, if it should exist!

It is my belief that somewhere along the line, likely around the time of Babel
and the confounding of tongues, the true religion passed down by Adam to Noah
and to Enoch was also confounded. Various disparate truths were rendered and
thrown around. Some cultures had some wonderful truths. Others had fewer. The
Hebrews (separate from the Jews, but I will write on that another time) had
perhaps the most truth? So the Biblical account was preserved and utilized by
God to spread the gospel throughout the ancient world. We have no reason to
believe that other such accounts do not exist alongside the Bible in the same
geographic location. The Book of Mormon, and especially the Jaredite texts,
prove otherwise.

What I'm saying is that we should potentially appreciate these pagan beliefs
more than pagans do! For, if you believe in the gospel as taught by the Church
of Jesus Christ of Latter-day Saints, we have the gifts to parse these truths.
Or rather, we can take the truths in the world and prayerfully integrate them
into our belief. Through personal revelation, the gift of the Holy Ghost, and
through the Priesthood, we can appreciate what beliefs our ancestors passed on
and what is being actively practiced by our brothers and sisters. God is
merciful!

**To my pagan brothers and sisters.** You are practicing an honorable trade. You
are preserving the holy traditions of our forefathers. Perhaps what I have wrote
may make you think and may convince you to at least give some scriptures a
thought. Please reach out to me if you are at all interested. I'd love to talk
to you. Unlike the Christendom of Medieval Europe, we are open to truths
throughout the world.

The Article of Faith number 13 states:

> We believe in being honest, true, chaste, benevolent, virtuous, and in doing
> good to all men; indeed, we may say that we follow the admonition of Paul---We
> believe all things, we hope all things, we have endured many things, and hope
> to be able to endure all things. **If there is anything virtuous, lovely, or
> of good report or praiseworthy, we seek after these things.**

_Emphasis added_. As such, if I am to adhere to this article of my faith, then I
ought to listen to you. I ought to listen to my ancestors as they speak thru the
grave. We members have a peculiar reverence with the dead, appropriately so. As
such I cannot simply ignore all traditions and myths before me without at least
looking into it. Because of the efforts of scholars, archaeologists, and
practitioners, we are able to have the preserved beliefs of many long dead
ancestors. And I am grateful for it!

## Future Writings

Future potential posts in this series include:
- The fields and halls (spiritual afterlife which occurs before the judgment
	and assignment to our kingdom)
- Pulling our ancestors across the rivers (proxy ordinances and work for the
	dead)
- The heroes who get to High Heaven (Celestial achievement)
	- This is might be the ultimate myth. The **Hero** with a thousand faces:
		**Jesus Christ**.


[^1]: Revelation 20:1-10, see also Doctrine and Covenants 43:31, and footnotes
  _c_ and _d_ for Doctrine and Covenants for 43:31
