---
title: "#GiveThanks Day 1 - Heavenly Father"
date: 2023-05-08T11:48:52-05:00
tags: ['givethanks', 'religion']
---

#### Following President Nelson's challenge

This last Sunday, during Gospel Study class, we discussed the Come Follow Me
lesson for the week. At the end, the teacher showed the video of President
Nelson asking the Church to share something they are thankful for each day for
seven days. This was originally back in November of 2020 during the height of
the Covid-19 pandemic. Although this is late for the original challenge, I will
follow the teachers prompt and write what I am thankful for here for this week.

## Day 1
### I am thankful for God
#### The Eternal Father, the Sky Father, the loving and caring Father who watches over us

The first Article Faith states the basic belief that "We believe in God, the
Eternal Father..." Further, however, as a member of the Church of Jesus Christ
of Later-day Saints, we believe that God is a separate being from Jesus Christ
and the Holy Ghost (Holy Spirit). According to Josephs Smith's account of his
*First Vision*, "...I saw two Personages, whose brightness and glory defy all
description, standing above me in the air. One of them spake unto me, calling me
by name, and said, pointing to the other—*This is My Beloved Son. Hear Him!*"

I am grateful that God cares for us each individually. Last night I watched a
movie about a woman who is searching for her child who she gave up as a young
woman in a nunnery (against her will). The two protagonists are the woman—who is
in her 70s as of the movie—and a journalist who was sacked recently and is
helping the woman find her son. The woman is an Irish Catholic and is very
devout, the journalist is—to no surprise—staunchly irreligious. In one exchange
the journalist talks about a headline he saw that stated something to the effect
of "God outdoes terrorists, thousands dead in earthquake."

Such an instance is not unique. The world is full of death and suffering. This
world is harsh and, at times, seemingly evil or antagonistic to our lives. How
then can God—especially a *loving* God—let such things happen? How can foul
death and suffering be allowed to exist if we are loved deeply by a supreme
creator? How could the devout Irish woman respond to such a question posed by
the journalist?

{{< img src=../images/god-the-father-ludovico-mazzolino.jpg caption=`God the
Father by Ludovico Mazzolino` >}}

As for me, my answer would be this, "We have our agency, so actions taken by man
to each other are permitted and not interrupted by God. Natural disasters can be
seen as trials and tribulations that, with time, can be overcome and better
individuals who endure them." A response may be, "How could God let a child
suffer from disease or terrible affliction such as molestation or abuse?" That's
when I would speak to the Plan of Happiness, "God knows all things that occur
and records all transgressions, especially those done to the innocent and
children. Justice will be fulfilled in the end. It is up to us to have the faith
to endure to see such a time. We must have faith that such justice *could* even
exist in the first place."

So, with this hypothetical conversation out of the way, I'll conclude that I
believe in God the Eternal, Heavenly, *Loving* Father. I thank Him each day for
my life. My breath and my blood come from Him. He is the supreme ruler of the
universe. He is also the supreme ruler of my soul. He permits me to live each
day and to change my life. In time, I can change and be like Him. In a very long
time that is.

{{< img src=../images/dyeus-pater.png caption=`A depiction of Dyeus Pater found
repeatedly in Survive The Jive's videos` >}}

His Son is evidence why He is a merciful and loving Father.
