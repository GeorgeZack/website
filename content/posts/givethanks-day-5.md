---
title: "#GiveThanks Day 5 - Temples"
date: 2023-05-12T21:45:12-05:00
tags: ['givethanks', 'religion']
---

## Day 5
### I am grateful for Temples, Houses of the Lord
#### Holiness to the Lord, The House of the Lord

The Temple is the most sacred place on the earth today. There are over 170
Temples currently operating in the world, and many many more are planned. The
Temple is a sacred place where I can worship my God and feel His presence.
Within the Temple I have made sacred Covenants with my God that bind me to Him.
I love the Lord, I love Jesus Christ, and I love the Holy Ghost. Within the
walls of the Temple I can feel their presence unlike anywhere else.

The Covenants and Ordinances within the Temple are opportunities to be connected
with the Savior. The Savior, in the Father's plan, plays an amazing and large
role. To add to the Atonement, the Covenants and Ordinances of the Temple are
integral for our Exaltation and connection to the Lord. And it is through Jesus
Christ that we can participate in such Covenants.

{{< img src=../images/salt-lake-temple-159320.jpg caption=`The Salt Lake City,
Utah, Temple` >}}

Not only can we have a chance at Exaltation, we can also give that chance, and
the chance of Salvation, to our kindred dead. Yesterday I talked about my
ancestors and love for them. I mentioned that in this Plan of Happiness we all
have the chance to hear the Gospel and to accept it no matter what generation we
were born to or what area of the world we live. This is achieved by the Temple.
Within the Temple we participate in many Covenants for ourselves, but only once.
When we have done the work for ourselves we can do the work by proxy for the
dead.

This is how we can be saved by our dead. By being able to go over and over again
to the Temple to learn more and more about the Covenants made by doing them for
the dead. I recently had the chance to work for about a year in the Nashville,
Tennessee, Temple. It was a great experience and I was able to go every other
Saturday. It was humbling to know that God trusted me enough to serve there, in
His House. I was able to assist patrons in their Ordinances and occasionally
participate in them myself.

{{< img src=../images/nashville-temple-lds.jpg caption=`The Nashville,
Tennessee, Temple` >}}

There were several occasions where the Spirit overwhelmed me. I know that what I
have done within the Temple will mark me forever. I am grateful that I can help
bind the world together through serving the dead. I love my ancestors and I can
honor them by serving in the Temple as often as possible. One day I shall meet
them in the spirit world and I shall be glad to see them, my kin, and know that
I was able to help them. I will not stop helping them in life or in death. The
Temple is a wonderful place.
