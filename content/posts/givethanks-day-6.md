---
title: "#GiveThanks Day 6 - Scriptures"
date: 2023-05-13T23:46:41-05:00
tags: ['givethanks', 'religion']
---

## Day 6
### I am thankful for the Scriptures
#### For the original authors and prophets, the translators, and the inspired people who preserved them

The Holy Scriptures comes in various translations and versions. I'm privy to the
King James Bible myself. First published in 1611, it is a masterwork of beauty
and sacred presentation. I believe that the most faithful translations of the
ancient Hebrew and Greek versions are found in the King James Bible. However,
any version so long as it is translated correctly can suffice. I believe the
Bible is inspired text and records of God's prophets and people throughout
history, as well as containing a testament of Jesus Christ from His disciples.
It is a true work.

{{< img src=../images/king-james-bible.jpg align=center caption=`The masterly
crafted King James Bible` >}}

So to is the Book of Mormon. It was translated by Joseph Smith Jr., who found
the plates through inspiration and angelic instruction. Though few believe in it
as I do in the world, and I have taken flak for believing in it, I cannot put
away the feelings and personal testimony I have developed of it over my life. It
is a sacred record and another testament of Jesus Christ.

Within the words of God we can hear Him. He can speak to us through the stories
and lessons of old. These are not simply stories though, they are given to use
through the words of prophets and apostles who were called by God. These men
(and occasionally women) were set apart for such a task and the Scriptures
record their works and their words. If we wish to hear Him in our lives we must
read of what has already been written by His servants. If we wish for God to
hear us we must pray.

{{< img src=../images/bible_bookofmormon.jpg caption=`The Two Sticks of the
Gospel` >}}

I am grateful to have the Scriptures in my life and to learn from them. In many
verses I have learned such important lessons. I have learned to trust in the
Lord, to trust Jesus Christ, to accept Baptism, to look to my God in all things,
to fear not, to rejoice in goodness, and to find the lost sheep. I have learned
that my Lord has sent His Son to save me:

> For the Son of man is not come to destroy men’s lives, but to save them.

-- Luke 9:56

To keep an Eternal perspective in mind during my life here on this Earth:

> Consider the lilies of the field, how they grow; they toil not neither do they
> spin: And I say unto you, That even Solomon in all his glory was not arrayed
> like one of these. Wherefore, if God so clothe the grass of the field, which
> to day is, and to morrow is cast into the oven, shall he not much more clothe
> you, O ye of little faith? ...Take therefore no thought for the morrow: for
> the morrow shall take thought for the things of itself. Sufficient unto the
> day is the evil thereof.

-- Matthew 6:28-30, 34

To turn to the Savior at all times:

> Come unto me, all ye that labour and are heavy laden, and I will give you
> rest. Take my yoke upon you, and learn of me; for I am meek and lowly in
> heart: and ye shall find rest unto your souls. For my yoke is easy, and my
> burden is light.

-- Matthew 11:28-30

To praise and bless the Lord:

> And said, Naked came I out of my mother’s womb, and naked shall I return
> thither: the LORD gave, and the LORD hath taken away; blessed be the name of
> the LORD.

-- Job 1:21

And to know truths eternal:

> Behold, I say unto you, wickedness never was happiness.

-- Alma 41:10
