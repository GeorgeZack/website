---
title: "Learning Rust"
date: 2022-08-17T17:03:28-05:00
tags: ['web', 'unix', 'suckless', 'rust']
---

I enjoy programming because I get to experiment with ideas, solve puzzles, and
create cool things. I like a language if it makes it easy to show off those
things. JavaScript, for as much as I like it and enjoy working in it, has a
fatal flaw for me. It cannot be easily shown off. You need a way to host the
HTML and embed the JavaScript in that HTML. You need a server to show off the
functionality of anything that isn't static.

Now it's not hard to do that. It's not hard to spin up a server, especially
after moving to Linux. It's not hard to write boilerplate or pull some from the
internet and use that. It's not difficult. But it's annoying to me just enough
that when I began to mentor someone wanting to learn programming, I did not
start with the language I knew best---JavaScript---and instead we went with C
and Python.

Enter Rust. I don't need to explain it because hundreds, if not thousands, have
explained it and have done a better job than I probably will. It's a high level
language that can be used to do system stuff basically. It's also really fun to
use! I can just type `cargo run` and my program starts in the command line. I
can write code and rerun the command to see the updates. Simple!

Of course, not everything has been perfect. Coming from JS I have to get used to
types and casting. Rust is a statically typed language so things are more exact.
But because the compiler is nice and user-friendly, I don't mind. It's been a
joy to learn and grow my abilities as a programmer. I have primarily worked on
web technology and with websites. That has been a blessing because it has helped
me learn so much. But I find it important to not stay in the web, I feel a drive
to push to learn other technology.

I've always wanted to learn C and have dabbled in it. I bought a Udemy course
for it which was fine if a little dull. But I really used it when I had to
customize my Suckless software suite. If you don't know,
[Suckless](https://suckless.org) is a ~~totally based and redpilled~~ group
dedicated to building software that sucks less, hence the name. The
configuration files for DWM and other software is the source code itself.
Genius! It's written in C. Big brained! C is pretty easy to modify in this
capacity and I have enjoyed learning just a little bit, enough to modify my
[configuration files](https://codeberg.org/GeorgeZack/gnu-configs) to my
preference.

Whenever looking at C, I usually came across people mentioning Rust. *Hmm*, I
thought, *this seems cool.* So one day I decided to redo an old biome generation
project written in JS using Node and Canvas to learn Rust. Now, a few weeks into
the project, I feel more comfortable with Rust but I still have a lot to learn.
The recreation project is coming along nicely and I get a sense that the
performance of the Rust version---lacking complete feature parity at the
moment---is much greater than the JS version. One motivator for me to learn Rust
and to avoid JS in the future was the NPM/Node ecosystem.

I talked briefly of that in the first post, but generally I don't like NPM. I
tried to run my JS biome generator and I couldn't even get the thing to start. I
had countless errors with outdated packages and such. I don't get the impression
that Rust will do the same. In fact, I've heard people say that if a crate
(roughly equivalent to a NPM package) hasn't been touched in 5 years it's not
because it's broken or abandoned, it's because it's done. WOW! What a shift in
mindset. Node is not like that at all! Good luck getting a two year old project
running with Node packages. Sad!

Anyways, Rust seems great so far, and I look forward to using it more. I enjoy
the ease of use for me as a developer and as a newcomer. I like it a lot, so
here's to learning more!
