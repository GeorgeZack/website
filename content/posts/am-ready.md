---
title: "I Am Ready"
date: 2024-03-09T10:55:36-06:00
tags: ['religion']
---

> I am ready.

--Paul von Hindenburg, in a telegram response to the German Emperor inquiring if
he was ready for employment. This was August 22nd, 1914, and the Great War had
just begun.[^1]

What is our response to war when it comes? For Marshal von Hindenburg it was
terse and simple. Thoughtful Faith recently (as of writing) published a video
entitled *Stop Making The Church Boring And Shallow.*[^2] The impetus for the
video, in Jacob Hansen's words, are "So I recently saw that the Saints
Unscripted podcast erased Kwaku from its thumbnails." Later he states "My whole
life almost all Latter-day Saints I have seen in the public square have been so
milquetoast, so wimpy, and it was so refreshing seeing a Latter-day Saint... go
on the offense and unapologetically engage in the public square."

Again, the video continues and Jacob shares the story of Brigham Young's
conversion (which I have expanded the include the entire story considering my
format):
> I recollect when I was young going to hear Lorenzo Dow preach. He was
> esteemed a very great man by the religious folks. I, although young in years
> and lacking experience, had thought a great many times that I would like to
> hear some man who could tell me something, when he opened the Bible, about the
> Son of God, the will of God, what the ancients did and received, saw and heard
> and knew pertaining to God and heaven. So I went to hear Lorenzo Dow. He stood
> up some of the time, and he sat down some of the time; he was in this position
> and in that position, and talked two or three hours, and when he got through I
> asked myself, 'What have you learned from Lorenzo Dow?' and my answer was,
> 'Nothing, nothing but morals.' He could tell the people they should not work
> on the Sabbath day; they should not lie, swear, steal, commit adultery, &c.,
> but when he came to teaching the things of God he was as dark as midnight... I
> would as lief go into a swamp at midnight to learn how to paint a picture and
> then define its colors when there is neither moon nor stars visible and
> profound darkness prevails, as to go to the religious world to learn about
> God, heaven, hell or the faith of a Christian. But they can explain our duty
> as rational, moral beings, and that is good, excellent as far as it goes.

--Brigham Young, in Journal of Discourses, 14:197-98

Let me reemphasize this: *What have you learned from Lorenzo Dow? ...Nothing,
nothing but morals.*

In the course of my life I have explored various religions and philosophies that
have done exactly this. Morals are taught, but nothing sinks down deeper than
that. Jordan Peterson, Carl Jung, Friedrich Nietzsche, et al. They speak to
morals and demand more from the reader, to elevate and change. But they are not
lasting. You are on your own when reading about these challenges. Sam Hyde talks
to catastrophe avoidance and gives good advice for careers, but he assumes that
you have the ability to change, to clean your room, then speaks to not ruining
your life after that. This is much closer and seems like continued guidance.

The issues that Lorenzo Dow has, and the good philosophers and comedian I
mentioned have, can not be sufficiently answered by the world or by this Earthly
experience. Or rather, this life speaks to something more than what is seen and
heard through our senses. Morals and doctrine can only get us *so* far. Even
reason and rationality. There are too many shaded places in the journey of life,
too many winds of change. We must be anchored to something. That something is
the rock of Christ.

Jacob Hansen is broadcasting his beliefs and doing a great job at it. He does
not want to be hid under a rock waiting for the end times. And I have decided
that neither do I.

<!--Don Bradley, author and historian, taught me something incredibly important
during his participation in a livestream hosted by the channel *Ward Radio*. The
livestream, *Ask Me Anything About Joseph Smith! (feat. Don Bradley)*[^3], is
very good. But the most important lesson is at around minute 29, second 49
(29:49). He is asked by an ex-member about if a particular source is reliable
but sets up the source with a story of Joseph Smith being defamed by traitors.
Essentially equating the source (Nauvoo Expositor) as a traitor whose witness of
events that have happened in the past and, therefore, should not be trusted as a
reliable source.

Don doesn't even answer the question. He cleanly and intelligently identifies
the issue with the question. The question is framed by a presupposition to make
it into a persuasive appeal. Historians, at least proper and honest ones, should
not approach a source with a conclusion already in mind. "The purpose of doing
history, foundationaly, is to know what happened in the past." He continues,
"...the proper question to bring to that is what happened."

This was an extremely important lesson. Since listening to this I have a huge
change of appetite for reading historical books. Historians parse primary
sources, interpret what has happened to produce a concise retelling---usually
faithful or as unbiased and close as possible to the facts known and
available---for a wider audience. A translation with notes and information with
context can also be provided. The historian is trained in the craft of reading
sources, comparing sources, translating at times when necessary, making notes of
all important details, then laying out these sources in a way that can be made
more clear.

Another important note is that history is not the study of what has happened in
the past, it is the study of what people from the past wrote down or otherwise
recorded. History, along with archaeology, provides a picture of the past. Often
when people read history books they take the conclusions from the historian *as
if it were fact* and not *fact according to the historian*. This is not a big
deal if we are in 1950's America, where academic institutions were upright and
held to high standards. Today, however, no such institution exists in my
eyes---besides perhaps, with a grain of salt, Brigham Young University---that I
would trust to produce honest historical texts. They do not produce historians
with the sufficient skills and ability to do so, instead these "historians" are
agents of an agenda.

Since the various cultural revolutions and social upheavals and changes of the
20th century, academic works have shifted from observation, faithful retellings,
understandings, and research into advocacy and activism. Take, for instance,
Christopher Columbus. He was once viewed as a patriotic, God fearing, honest man
who explored the New World.[^4] Now he is reviled[^5] and hated.[^6] Why? Why
has he changed from hero to villain? Look at the publishing history of
biographies on the man. First texts such as {} are published in {}, later books
like *Christopher Columbus: A People's History of the United States* by Howard
Zinn in 1981 and *Lies My Teacher Told Me* by James W. Lowen in 1995 are
published. Both the later books present a vision of Christopher Columbus as a
villain, while the former, a hero.

Often times we see his own words twisted. Often times sites, such as
*Understanding Prejudice* will cite Christopher Columbus' "own" words to show
him as a slaver.[^7] The quote "They should be good servants..." is taken out of
context and translated incompletely. The true quote is "...should be good and
intelligent servants...",[^8] [^9] implying that the natives Christopher is
talking to respond quickly to him and must be good servants to their king.
Unfortunately such websites as Understanding Prejudice, funded with grants from
the National Science Foundation,[^10] will not be corrected or open to
correction. Instead it is about an agenda and the forwarding of such, rather
than the honest framing of good questions of "what happened" in history.-->

[^1]: Hindenburg, Paul von. *Out of My Life*. Translated by F. A. Holt. Cassell
and Company, Ltd., 1920. Page 81. Accessed March 6, 2024. URL:
https://archive.org/details/outofmylife00hinduoft/page/n5/mode/2up
[^2]: https://youtu.be/H4IvD3Ws5rk?si=9dG5MdPo0xT3YLOq
[^3]: https://www.youtube.com/live/hwvkPtOw9Ao?si=N0B6nbvdpF3X-Tos
[^4]: asdf
[^5]: asdf
[^6]: asdf
[^7]: https://secure.understandingprejudice.org/nativeiq/columbus.htm
[^8]: Columbus, Christopher. The Diario of Christopher Columbus's First Voyage
to America, 1492-1493, translated by Oliver Dunn and James E. Kelley, Jr. Pages
66-69. Accessed March 8, 2024. URL:
https://books.google.com/books?id=nS6kRnXJgCEC&printsec=frontcover&source=gbs_ViewAPI#v=twopage&q&f=false
[^9]: Cummins, John, ed. The Voyage Of Christopher Columbus. Columbus’ Own
Journal Of Discovery And. Page 94. Accessed March 8, 2024. URL:
https://archive.org/details/thevoyageofchristophercolumbus.columbusownjournalofdiscoveryandbyjohncummins/page/n117/mode/2up
[^10]: https://secure.understandingprejudice.org/about/
