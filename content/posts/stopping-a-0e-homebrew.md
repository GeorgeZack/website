---
title: "Stopping a 0e Homebrew"
date: 2023-07-10T23:28:01-05:00
tags: ['rpgs']
---

Recently I've been working on my homebrew that I talked about
[in another post]({{< ref "beginning-an-0e-d&d-homebrew.md" >}}) and I realized
something. I could be reading T1: The Village of Hommlet or B2: Keep on the
Borderlands instead of spinning my wheels on what armor should weigh, what type
of encumbrance system should I use, and what abstract unit of both volume and
weight should be used (or even separate them in the first place). My capacity
for technical creativity (as apposed to artistic creativity like writing) is
limited in a day. I program and want to do well in it. I want to expand my
knowledge of technology and proper programming. I have decided that I can't do
that *and* write a wheel spinning manual of rules for my RPG that may never even
see the light of day.

So here is what I had so far: [behold!](/rules.html)

I like percentile games and wanted to use that. I liked adding in more
attributes but completely removing skills. I liked the 3 class dichotomy of
Fighter, MU, and Cleric so I kept something like it in. Really, when looking at
the rules, I want to play them. But I don't want to make them.

Perhaps I will start again and stop again some day. Perhaps I'll release them in
a final form. Until then, here's an update and a pause. I want to run Blueholme
or S&W and play in B2 or T1.

That's all.
