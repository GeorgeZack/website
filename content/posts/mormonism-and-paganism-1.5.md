---
title: "Mormonism and Paganism 1.5"
date: 2022-11-07T22:44:48-06:00
tags: ['religion']
---

# HWÆT

I recently began reading Beowulf. It's a grand poem, and I'm sure you already
know what it is. I've continued to research what exactly my ancestors were up to
in life. What did they believe and see? Beowulf is not necessarily religious,
but it does contain many religious artifacts and facts that indicate what the
Anglo-Saxons and iron-age Nordic peoples believed. The Christianization of the
North is a hot topic, and there are a myriad of views on the subject. But one
thing is for certain, that Beowulf itself is from a Christian author(s)[^1].

What does this mean? For one, we lose detail on the beliefs of those who
originally orated the story and the sources from which it is drawn. But in
another sense, we realize that this story is important to Christians in England
at this time. So what is important about Grendel and the monsters of Cain? Why
should we care about Beowulf the boastful? I endeavor to find out.

And I also will try to update the site quicker this time.

[^1]: We may never know who the author was, or if there were multiple authors. I
  hope to meet him, or them, one day.
