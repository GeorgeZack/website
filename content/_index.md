---
title: Welcome Frens
---

## Stay a while, and listen!

{{< img src=./stalker-fire.gif caption=`Enjoy the campfire` >}}
{{< audio src="STALKER-campfire.flac" fallback="STALKER-campfire.mp3" >}}

\
\
\
\
Take a look around and enjoy your stay.

The internet can be a bad place full of bad things.

I hope this is a good place with good things for you, fren.

{{< img src=./rare-pepe-magic.gif >}}
