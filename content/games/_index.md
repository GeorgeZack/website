---
title: "LAN Party Games"
date: 2023-04-25T20:14:29-05:00
---

Emphasis on multiplayer games, for use in LAN parties, with no DRM. This almost
exclusively excludes all Steam games unless they are F2P. Such a list of F2P
Steam multiplayer games will not be included here.

| Acronym | Description              |
|---------|--------------------------|
| ARPG    | Action Role-playing Game |
| FPS     | First Person Shooter     |
| RPG     | Role-playing Game        |
| RTS     | Real-time Strategy       |
| TBS     | Turn-based Strategy      |

&nbsp;

## FOSS

| Title                         | Genre        | Players |
|-------------------------------|--------------|---------|
| 0AD                           | RTS          |         |
| Beyond All Reason             | RTS          |         |
| Bitfighter                    | Action       |         |
| CS2D                          | Action       |         |
| FreeCiv                       | TBS          |         |
| FreeOrion                     | TBS          |         |
| Hedgewars                     | TBS          |         |
| MechWarrior: Living Legends   | Action/FPS   |         |
| NoX                           | RPG          |         |
| OpenAge (not playable/in dev) | RTS          |         |
| OpenMW                        | RPG          |         |
| OpenSpades                    | FPS          |         |
| Space Nerds In Space          | Simulation   |         |
| SuperTuxKart                  | Racing       |         |
| Teeworlds                     | Action       |         |
| Unvanquished                  | FPS/RTS      |         |
| Vintage Story                 | RPG/Survival |         |
| Xonotic                       | FPS          |         |
| Zandronum (Doom Multiplayer)  | FPS          |         |

&nbsp;

## GOG

| Title                                          | Genre          | Players |
|------------------------------------------------|----------------|---------|
| AI War (Fleet Command & 2)                     | RTS            |         |
| ARMA: Cold War Assault                         | FPS/Simulation |         |
| Battle Realms + Winter of the Wolf             | RTS            |         |
| Crysis Wars                                    | FPS            |         |
| Delta Force: Black Hawk Down                   | FPS            |         |
| Divinity: Dragon Commander                     | RTS/Simulation |         |
| Doom (many WADs)                               | FPS            |         |
| Duke Nukem 3D Atomic Edition                   | FPS            |         |
| Dungeon Keeper Gold                            | RTS            |         |
| Empire Earth Gold Edition                      | RTS            |         |
| Factorio                                       | Survival       |         |
| Far Cry (1 & 2)                                | FPS            |         |
| Field of Glory II                              | TBS            |         |
| Freespace 2                                    | FPS/Simulation |         |
| Grim Dawn                                      | ARPG           | 4       |
| IL-2 Sturmovik: 1946                           | Simulation     |         |
| Imperium Galactica II: Alliances               | TBS/RTS        |         |
| Medal of Honor                                 | FPS            |         |
| Morrowind (OpenMW)                             | RPG            |         |
| Mount and Blade (Warband & With Fire & Sword)* | Action         |         |
| Neverwinter Nights (1 & 2)                     | RPG            |         |
| Parkitect                                      | Casual         |         |
| Quake III Arena                                | FPS            |         |
| S.T.A.L.K.E.R. (SoC, CS, CoP)                  | FPS            |         |
| Serious Sam: The Second Encounter              | FPS            |         |
| Star Wars Battlefront (Classic, 1 & 2)         | FPS/TPS        |         |
| Star Wars: Jedi Knight - Jedi Academy          | TPS/Action     |         |
| Stronghold (HD & Crusader HD)                  | RTS            |         |
| Supreme Commander (FAF)                        | RTS            |         |
| Terraria                                       | RPG/Survival   |         |
| Torchlight II                                  | ARPG           |         |
| Total Annihilation (& Kingdoms)                | RTS            |         |
| Two Worlds                                     | RPG            | 2       |
| Unepic                                         | RPG            |         |
| Unreal Gold                                    | FPS            |         |
| Unreal Tournament 2004                         | FPS            |         |
| War for the Overworld                          | RTS            |         |
| Warlords Battlecry 3                           | RTS            |         |
| World in Conflict                              | RTS            |         |

\* Multiplayer requires serial keys for connection. However, serial keys can be
found online.

&nbsp;

### GOG Wishlist

- Din's Curse/Zombasite
    - Up to 16 players
    - $10 for Din's Curse
    - $20 for Zombasite
