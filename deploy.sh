#!/bin/sh
USER="admin"
HOST=96.126.100.146
DIR="/home/admin/website/public" # the directory where your web site files should go

hugo && rsync -avz --delete public/ ${USER}@${HOST}:${DIR}

exit 0
