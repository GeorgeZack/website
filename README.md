# website

## Requirements

`hugo`

## Development

`hugo server` or `hugo server -D`

To create a new post:\
`hugo new content posts/name.md`

## Deploy

Use `bash` or such to execute the `deploy.sh` file included

## Description

Website development repository.

UPDATED: No longer using configuration tool, just need to upload my own files to
the repo.

[DigitalOcean NGINX Configuration](https://www.digitalocean.com/community/tools/nginx?domains.0.server.domain=georgesz.xyz&domains.0.server.path=%2Fhome%2Fadmin%2Fwebsite&domains.0.https.certType=custom&domains.0.php.php=false&domains.0.routing.index=index.html&domains.0.routing.fallbackHtml=true&domains.0.routing.fallbackPhp=false&global.https.ocspGoogle=false&global.nginx.pid=)

## Rathole

DOCUMENTATION NEEDED

